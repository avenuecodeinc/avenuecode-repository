package com.avenuecode.functionaltesting.actions;

import java.io.File;
import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;

public class BaseTest extends ReadProperties {

	public static String url;
	public static String url_Reoport;
	public static String userName;
	public static String passWord;
	public static WebDriver driver;
	public static ExtentReports report;
	protected ExtentTest logger;

	public BaseTest() throws IOException {
		super();

		url = ReadProperties.getUrl();
		userName = ReadProperties.getUserName();
		passWord = ReadProperties.getPassword();

		// Initialize Objects for advance report
		String fileName = "./advancedReport.html";
		File file = new File(fileName);
		url_Reoport = file.getAbsolutePath();
		report = new ExtentReports(fileName);
		logger = report.startTest(this.getClass().getName().toString());

	}

	public void generateReport() {

		report.endTest(logger);
		report.flush();
		String url_concat = "file://" + url_Reoport;
		driver.get(url_concat);
		driver.findElement(By.xpath("//*[@id='enableDashboard']/i")).click();

	}

}
