package com.avenuecode.functionaltesting.actions;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

public class BrowserType extends BaseTest {

	public BrowserType() throws IOException {
		super();

	}

	public WebDriver SetupBrower(WebDriver driver, String browser, String url) {

		if (browser.equalsIgnoreCase("firefox")) {
			driver = new FirefoxDriver();
		}

		else if (browser.equalsIgnoreCase("chrome")) {

			{
				System.setProperty("webdriver.chrome.driver", "FilePath/chromedriver.exe");
			}
			driver = new ChromeDriver();
		}

		else if (browser.equalsIgnoreCase("ie")) {

			{
				System.setProperty("webdriver.ie.driver", "FilePath/IEDriverServer.exe");
			}
			driver = new InternetExplorerDriver();
			{
				DesiredCapabilities iecapabilities = DesiredCapabilities.internetExplorer();
				iecapabilities.setCapability(InternetExplorerDriver.IE_ENSURE_CLEAN_SESSION, true);
			}
		}

		// Implicit Wait and Maximize browser
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().window().maximize();

		// Navigate to URL
		driver.get(url);
		return driver;

	}
}
