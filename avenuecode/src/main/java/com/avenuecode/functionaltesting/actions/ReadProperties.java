package com.avenuecode.functionaltesting.actions;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class ReadProperties {

	static Properties _properties;

	private final static String AvenueCode_Url = "url";
	private final static String AvenueCode_UserName = "userName";
	private final static String AvenueCode_PassWord = "password";

	public ReadProperties() throws IOException {

		_properties = new Properties();
		FileInputStream fs = new FileInputStream("test.properties");

		_properties.load(fs);
		System.out.println(_properties);

	}

	public static String getUrl() {
		return getStringForKey(AvenueCode_Url);
	}

	public static String getUserName() {
		return getStringForKey(AvenueCode_UserName);
	}

	public static String getPassword() {
		return getStringForKey(AvenueCode_PassWord);
	}

	public static String getStringForKey(String key) {
		return _properties.getProperty(key);
	}
}
