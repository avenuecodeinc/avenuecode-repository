package com.avenuecode.functionaltesting.pages;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.avenuecode.functionaltesting.actions.BaseTest;
import com.relevantcodes.extentreports.LogStatus;

public class MyLoginPage {

	WebDriver driver;
	@FindBy(xpath = "html/body/div[1]/div[1]/div/div[2]/ul[2]/li[1]/a")
	private WebElement singIn_link;

	@FindBy(id = "user_email")
	private WebElement email_textBox;

	@FindBy(id = "user_password")
	private WebElement password_textBox;

	@FindBy(xpath = "//*[@id='new_user']/input")
	private WebElement singIn_Button;

	@FindBy(xpath = "//*[@id='user_remember_me']")
	private WebElement rememberMe_checkBox;

	public MyLoginPage(WebDriver driver) {
		this.driver = driver;

	}

	public void LoginPageMethod(String UserName, String Password) {
		singIn_link.click();
		email_textBox.sendKeys(UserName);
		password_textBox.sendKeys(Password);
		singIn_Button.click();

	}
}
