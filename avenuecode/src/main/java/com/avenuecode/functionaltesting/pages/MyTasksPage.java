/**
 * 
 */
package com.avenuecode.functionaltesting.pages;

import java.io.IOException;
import java.util.Date;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import com.avenuecode.functionaltesting.actions.BaseTest;
import com.relevantcodes.extentreports.LogStatus;

/**
 * @author exilant
 *
 */
public class MyTasksPage extends BaseTest {
	public MyTasksPage() throws IOException {
		super();
	}

	WebDriver driver;

	@FindBy(xpath = "html/body/div[1]/div[1]/div/div[2]/ul[1]/li[2]/a")
	private WebElement MyTasks_link;
	
	
	
	@FindBy(xpath = "html/body/div[1]/div[2]/div[2]/div/table/tbody/tr/td[4]/button")
	private WebElement MySubTask_link;
	
	@FindBy(xpath = "//*[@id='edit_task']")
	private WebElement EditSubTask;
	
	@FindBy(xpath = "//*[@id='new_sub_task']")
	private WebElement subTaskDescription;
	
	@FindBy(xpath = "//*[@id='add-subtask']")
	private WebElement addSubTask_Button;
	
	@FindBy(xpath = "html/body/div[4]/div/div/div[3]/button")
	private WebElement closeButton;

	@FindBy(id = "new_task")
	private WebElement addNewTask_textBox;

	@FindBy(css = "html/body/div[1]/div[3]/div[1]/form/div[2]/span")
	private WebElement addNewTask_Button;

	@FindBy(xpath = "html/body/div[1]/h1")
	private WebElement welcome_text;

	public void AddNewTasksMethod() {

		Assert.assertTrue(true, MyTasks_link.toString());
		logger.log(LogStatus.INFO, "Found Object: " + MyTasks_link.getText());
		MyTasks_link.click();

		Date sysDate = new Date();
		String dateString = sysDate.toString();
		Assert.assertEquals(welcome_text.getText(), "amaresh's ToDo List");
		logger.log(LogStatus.INFO, "ToDo List Name:  " + welcome_text.getText());
		logger.log(LogStatus.WARNING, "Just a warning message to the user");
		addNewTask_textBox.sendKeys("NewTask_" + dateString);
		addNewTask_textBox.sendKeys(Keys.ENTER);
		logger.log(LogStatus.PASS, "Successfully created new task");
	}

	public void AddNewSubTasksMethod() {


		Date sysDate = new Date();
		String dateString = sysDate.toString();
		
		Assert.assertTrue(true, MyTasks_link.toString());
		logger.log(LogStatus.INFO, "Found Object: " + MyTasks_link.getText());
		MyTasks_link.click();
		MySubTask_link.click();
		
//		EditSubTask.clear();
//		EditSubTask.sendKeys("SubTaskEdited_" + dateString);
		subTaskDescription.clear();
		subTaskDescription.sendKeys("SubTaskEdited_" + dateString);
		logger.log(LogStatus.INFO, "Description has been added in subTask" );
		
		addSubTask_Button.click();
		closeButton.click();
		
	}

}
