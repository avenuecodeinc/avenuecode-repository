package com.avenuecode.functionaltesting.test;

import java.io.IOException;

import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.avenuecode.functionaltesting.actions.BrowserType;
import com.avenuecode.functionaltesting.pages.MyLoginPage;
import com.avenuecode.functionaltesting.pages.MyTasksPage;

public class TestCases extends BrowserType {

	MyTasksPage myTaskPage;
	MyLoginPage loginPage;

	public TestCases() throws IOException {
		super();
	}

	@BeforeClass
	public void SetUpBrowser() {
		driver = super.SetupBrower(driver, "firefox", url);
		myTaskPage = PageFactory.initElements(driver, MyTasksPage.class);
		loginPage = PageFactory.initElements(driver, MyLoginPage.class);

	}

	@Test(priority = 1)
	public void VerifyloginPageTest() {
		loginPage.LoginPageMethod(userName, passWord);

	}

	@Test(priority = 2)
	public void VerifyAddNewTasksTest() {
		myTaskPage.AddNewTasksMethod();

	}

	@Test(priority = 3)
	public void VerifyAddNewSubTasksTest() {
		myTaskPage.AddNewSubTasksMethod();

	}
	
	
	@AfterClass
	public void getReport() {
		generateReport();
	}

}
